package ru.whitewarrior.glsl;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;

import static com.intellij.psi.TokenType.BAD_CHARACTER;
import static com.intellij.psi.TokenType.WHITE_SPACE;
import static ru.whitewarrior.glsl.lang.psi.ShaderTypes.*;

%%

%{
  public _ShaderLexer() {
    this((java.io.Reader)null);
  }
%}

%public
%class _ShaderLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode

EOL=\R
WHITE_SPACE=\s+

IDENTIFIER=[a-zA-Z_][a-zA-Z0-9_]*
COMMENT="/"\*[\w\S\s]*\*"/"

%%
<YYINITIAL> {
  {WHITE_SPACE}      { return WHITE_SPACE; }


  {IDENTIFIER}       { return IDENTIFIER; }
  {COMMENT}          { return COMMENT; }

}

[^] { return BAD_CHARACTER; }
