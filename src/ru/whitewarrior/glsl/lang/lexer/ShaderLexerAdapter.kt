package ru.whitewarrior.glsl.lang.lexer

import com.intellij.lexer.FlexAdapter
import ru.whitewarrior.glsl._ShaderLexer

class ShaderLexerAdapter : FlexAdapter(_ShaderLexer(null))