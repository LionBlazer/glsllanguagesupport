package ru.whitewarrior.glsl.lang

import com.intellij.lang.Language

object ShaderLanguage : Language("GLSL")