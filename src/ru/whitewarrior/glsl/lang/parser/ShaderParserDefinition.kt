package ru.whitewarrior.glsl.lang.parser

import com.intellij.lang.ASTNode
import com.intellij.lang.ParserDefinition
import com.intellij.lang.PsiParser
import com.intellij.lexer.Lexer
import com.intellij.openapi.project.Project
import com.intellij.psi.FileViewProvider
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.TokenType
import com.intellij.psi.tree.IFileElementType
import com.intellij.psi.tree.TokenSet
import ru.whitewarrior.glsl.lang.ShaderLanguage
import ru.whitewarrior.glsl.lang.lexer.ShaderLexerAdapter
import ru.whitewarrior.glsl.lang.psi.ShaderFile
import ru.whitewarrior.glsl.lang.psi.ShaderTypes


class ShaderParserDefinition : ParserDefinition {
    override fun createParser(project: Project?): PsiParser = ShaderParser()

    override fun createFile(project: FileViewProvider): PsiFile = ShaderFile(project)

    override fun getStringLiteralElements(): TokenSet = TokenSet.EMPTY

    override fun getFileNodeType(): IFileElementType = FILE

    override fun createLexer(project: Project?): Lexer = ShaderLexerAdapter()

    override fun createElement(project: ASTNode?): PsiElement = ShaderTypes.Factory.createElement(project)

    override fun getCommentTokens(): TokenSet = COMMENTS

    override fun getWhitespaceTokens(): TokenSet = WHITE_SPACES

    override fun spaceExistenceTypeBetweenTokens(left: ASTNode, right: ASTNode): ParserDefinition.SpaceRequirements =
        ParserDefinition.SpaceRequirements.MAY

    companion object {
        private val WHITE_SPACES = TokenSet.create(TokenType.WHITE_SPACE)
        private val COMMENTS = TokenSet.create(ShaderTypes.COMMENT)

        private val FILE = IFileElementType(ShaderLanguage)
    }
}