package ru.whitewarrior.glsl.lang.highlight

import com.intellij.lexer.Lexer
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.editor.HighlighterColors
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors
import com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey
import com.intellij.psi.tree.IElementType
import ru.whitewarrior.glsl.lang.lexer.ShaderLexerAdapter
import ru.whitewarrior.glsl.lang.psi.ShaderTypes


class ShaderSyntaxHighlighter : SyntaxHighlighterBase(){

    override fun getHighlightingLexer(): Lexer {
        return ShaderLexerAdapter()
    }

    override fun getTokenHighlights(tokenType: IElementType): Array<out TextAttributesKey?> {
        return when (tokenType) {
            ShaderTypes.IDENTIFIER -> IDENTIFIER_KEYS
            ShaderTypes.COMMENT -> COMMENT_KEYS
            else -> IDENTIFIER_KEYS
        }
    }

    companion object {
        private val IDENTIFIER = createTextAttributesKey("SHADER_DEFINITION", DefaultLanguageHighlighterColors.IDENTIFIER)
        private val COMMENT = createTextAttributesKey("SHADER_COMMENT", DefaultLanguageHighlighterColors.BLOCK_COMMENT)
        private val BAD_CHARACTER = createTextAttributesKey("SHADER_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER)

        private val BAD_CHAR_KEYS = arrayOf(BAD_CHARACTER)
        private val IDENTIFIER_KEYS = arrayOf(IDENTIFIER)
        private val COMMENT_KEYS = arrayOf(COMMENT)
        private val EMPTY_KEYS = arrayOfNulls<TextAttributesKey>(0)
    }
}