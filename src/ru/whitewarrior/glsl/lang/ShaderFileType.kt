package ru.whitewarrior.glsl.lang

import com.intellij.openapi.fileTypes.LanguageFileType
import ru.whitewarrior.glsl.graphic.icon.ShaderIcons
import javax.swing.Icon

object ShaderFileType: LanguageFileType(ShaderLanguage) {
    override fun getIcon(): Icon = ShaderIcons.MAIN

    override fun getName(): String = "GLSL file"

    override fun getDefaultExtension(): String = "glsl"

    override fun getDescription(): String =
            "OpenGL shader"
}