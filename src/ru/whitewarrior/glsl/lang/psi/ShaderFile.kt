package ru.whitewarrior.glsl.lang.psi

import com.intellij.extapi.psi.PsiFileBase
import com.intellij.lang.Language
import com.intellij.openapi.fileTypes.FileType
import com.intellij.psi.FileViewProvider
import ru.whitewarrior.glsl.lang.ShaderFileType
import ru.whitewarrior.glsl.lang.ShaderLanguage
import javax.swing.Icon



class ShaderFile(viewProvider: FileViewProvider) : PsiFileBase(viewProvider, ShaderLanguage) {
    override fun getFileType(): FileType {
        return ShaderFileType
    }

    override fun toString(): String {
        return "Shader File"
    }
}