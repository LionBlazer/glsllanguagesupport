package ru.whitewarrior.glsl.lang.psi

import com.intellij.psi.tree.IElementType
import ru.whitewarrior.glsl.lang.ShaderLanguage

class ShaderTokenType(debugName: String) : IElementType(debugName, ShaderLanguage)