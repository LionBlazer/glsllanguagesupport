package ru.whitewarrior.glsl.graphic.icon

import com.intellij.openapi.util.IconLoader


object ShaderIcons {
    var MAIN = IconLoader.getIcon("icons/glsl.png")
    var MAIN_CENTER = IconLoader.getIcon("icons/in.png")
    var BUILT_IN = IconLoader.getIcon("icons/builtin.png")
}